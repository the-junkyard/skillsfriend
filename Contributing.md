## Creating a database


`docker run --detach --name skillsfriend --env MARIADB_USER=skillsfriend --env MARIADB_PASSWORD=skillsfriend --env MARIADB_ROOT_PASSWORD=skillsfriend  mariadb:latest`

## testing the connection


1. Install maria utils 

`sudo apt install mariadb-client-core-10.3` 

2. Find the network that docker's on

`ip a | grep docker -A 2`

```
andrew@vanguard:~/repos$ ip a | grep docker -A 2
21: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default 
    link/ether 02:42:df:d5:6c:30 brd ff:ff:ff:ff:ff:ff
    inet 192.168.64.1/24 brd 192.168.64.255 scope global docker0    <------- SUBNET HERE
       valid_lft forever preferred_lft forever
    inet6 fe80::42:dfff:fed5:6c30/64 scope link 
--
79: vetha468822@if78: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master docker0 state UP group default 
    link/ether 36:22:77:6d:8a:75 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::3422:77ff:fe6d:8a75/64 scope link 
--
81: veth716a067@if80: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master docker0 state UP group default 
    link/ether 42:fb:66:41:ae:c1 brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::40fb:66ff:fe41:aec1/64 scope link
```


3. find the IP that nysql's on

`nmap 192.168.64.0/24 | grep -B 4 3306`

```
andrew@vanguard:~/repos/repos$ nmap 192.168.64.0/24 | grep -B 4 3306
Nmap scan report for 192.168.64.3   <----------- IP OF SQL SERVER
Host is up (0.00016s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
3306/tcp open  mysql
```

4. test connecting to sql server

mysql -h 192.168.64.3 -u skillsfriend -pskillsfriend

5. create the initial database 

`mysql -h 192.168.64.3 -u root -pskillsfriend < skillsfriend/create_database.sql`

(No output)


6. check the user can access

`mysql -h 192.168.64.3 -u skillsfriend -pskillsfriend -b skills_friend`


Output: 

```
andrew@vanguard:~/repos/repos/skillsfriend$ mysql -h 192.168.64.3 -u skillsfriend -pskillsfriend -b skills_friend
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 5
Server version: 10.11.3-MariaDB-1:10.11.3+maria~ubu2204 mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [skills_friend]> 
```

7. install required dependencies for Python to run

```bash
sudo apt purge libmariadb3 
sudo apt purge libmariadb-dev

curl -O https://downloads.mariadb.com/Connectors/c/connector-c-3.3.4/mariadb-connector-c-3.3.4-ubuntu-focal-amd64.tar.gz

sudo su

sudo tar xvf mariadb-connector-c-3.3.4-ubuntu-focal-amd64.tar.gz --directory /usr --strip-components 1

echo "/usr/lib/mariadb/" > /etc/ld.so.conf.d/mariadb.conf
ldconfig

exit

python3 -m pip install --user mariadb
```



