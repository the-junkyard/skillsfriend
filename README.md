It's mid 2023. I've got rusty with SQL, database design and I want to pick this up again. 

So i'm making a little app I think. The goal here is to make an application with which you can add your skills and add experiences which have demonstrated those skills. 

a "SkillsFriend"



Okay so the database looks roughly like this. This is a bit old now but the jist is there: 

![An entity relationship diagram showing Skill, Experience and SkillCategory tables, with two tables linking skill to each of the other tables.](./erd-updated-with-relations.png)

Okay so how am I approaching this? 

First I've designed the ERD above, then I've written and tested the SQL to load that data.

Next I need some content to put into the database! Luckily I've been looking for an excuse to write about some experiences I've had and how I've learned from them.

Luckily I [spent some time in 2020](listing a load of my skills and experiences from previous jobs, so I've got _some_ content from past jobs, but I could do with a load more from JR.)

But that's not a job needs doing right now, so lets use our historical experiences and skills as the first tranche of content. 

First I need to put it all into a.. machine readable format. so lets do that:

- [Skill Levels](./skill_levels.yaml)

- [Skill Categories](./skill_categories.yaml) - Descriptions not yet done

- [Skills](./skill.yaml) - This isn't finished yet!


# THE NEXT STEP:
I'm putting this down to pick up writing a bit about a CV over in [my website repo](https://github.com/andpatt/acpatt.com)

The next step here is to create some yamls with content in them - content for every table, basically, then to  figure out how to load that data into the database. 

------------------

# 2023/05/21

So I've got content for skills, categories, levels and mappings of skills to categories. Before I start writing experiences, I wanna load what I've got into the db. That means using Python to read the yaml, then somehow parse it, and use the mariadb pip package to insert the data. 

After _that_ Ima try something more interesting. Something like SQLAlchemy.

But first, use mariadb direct. 

## 09:59. 

Got to the stage where I Can load the yamls into the db when they're simple formatted. Now I gotta work on the more complex skills & mappings load. 

## 15:33

Not been working all day, like, but maybe an hour. I've got as far as putting the skills into a dataframe. I'm going to try load that into the database now. 