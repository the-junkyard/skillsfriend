create database skills_friend;

use skills_friend

CREATE TABLE skill_level (
  ID int NOT NULL AUTO_INCREMENT UNIQUE,
  name varchar(255) NOT NULL UNIQUE,
  description MEDIUMTEXT,
  PRIMARY KEY (ID)
);

CREATE TABLE skill (
  ID int NOT NULL AUTO_INCREMENT UNIQUE,
  name varchar(255) NOT NULL UNIQUE,
  description MEDIUMTEXT,
  skill_level_id int,
  PRIMARY KEY (ID),
  FOREIGN KEY (skill_level_id) references skill_level(ID)
);

CREATE TABLE skill_category (
  ID int NOT NULL AUTO_INCREMENT UNIQUE,
  name varchar(255) NOT NULL UNIQUE,
  description MEDIUMTEXT,
  PRIMARY KEY (ID)
);

CREATE TABLE experience (
  ID int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  end_date date,
  situation MEDIUMTEXT,
  task MEDIUMTEXT,
  action MEDIUMTEXT,
  result MEDIUMTEXT,
  PRIMARY KEY (ID)
);

CREATE TABLE skill_skill_category (
  category_id int NOT NULL,
  skill_id int NOT NULL,
  PRIMARY KEY (category_id, skill_id),
  FOREIGN KEY (category_id) references skill_category(ID),
  FOREIGN KEY (skill_id) references skill(ID)
);

CREATE TABLE experience_skill_evidence (
  experience_id int NOT NULL,
  skill_id int NOT NULL,
  PRIMARY KEY (experience_id, skill_id),
  FOREIGN KEY (experience_id) references experience(ID),
  FOREIGN KEY (skill_id) references skill(ID)
);

GRANT ALL PRIVILEGES ON skills_friend.* TO 'skillsfriend'@'%';