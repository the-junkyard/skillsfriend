#!/usr/bin/bash

set -e


echo "killing the old SQL server"
docker stop skillsfriend && docker rm skillsfriend

echo "starting a new one"

docker run --detach --name skillsfriend --env MARIADB_USER=skillsfriend --env MARIADB_PASSWORD=skillsfriend --env MARIADB_ROOT_PASSWORD=skillsfriend  mariadb:latest

echo "getting the IP address"

container_ip=$(docker inspect skillsfriend | jq .[].NetworkSettings.Networks.bridge.IPAddress -r)

echo "waiting for the server to be up"

while ! docker logs skillsfriend 2>&1 | grep "ready for connections"
do
    sleep 1
    echo "still waiting"
done

# it's still not ready when it says it is



sleep 4

echo "populating schema"

mysql -h $container_ip -u root -pskillsfriend < create_database.sql



echo "Loading data into database"

jupyter nbconvert --to notebook --execute load_content.ipynb

echo "Ready for use".
