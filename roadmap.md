This is a list of extra features we could add to this little app: 

- add "role" that an experience relates to. 
- add "privacy" of an experience, allowing it to be hidden. 
- add the _amount_ that an experience evidences a skill.
- add related skills.
- add "tools" that have a one to many relationship with skills? So like Terraform is a tool that's part of IaC? Or is Tool more like "skill"? are they related?

What to do when one skill is actually at the level of another category?

- Summary of "where I am with X skill" so that I can see at a glance how good I am. That would then be updated whenever I added an experience! Nice job, me..

